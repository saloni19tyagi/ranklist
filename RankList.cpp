#include<iostream>
#include<fstream>
#include<vector>

using namespace std;

bool sortbysec(const pair<int,int> &a, const pair<int,int> &b){ 
    return (a.second < b.second); 
} 

int main(){
    string rollnumber;
    int marks;
    int NofStudents;
    vector < pair <string , int> > ranks;
    cin >> NofStudents;
    
    ifstream MarkList;
    MarkList.open("marklist.txt");
    char output[100];
    if (MarkList.is_open()) {
        while (!MarkList.eof()) {
           ranks.push_back(make_pair(rollnumber,marks));
    }
        sort(ranks.begin() , ranks.end() , sortbysec);
    
        cout << 1 << ranks[0].first << ranks[0].second << endl;
    
        for (int i = 1 ; i < NofStudents ; i++){
            if (ranks[i].second == ranks[i-1].second)
                cout << " " << ranks[i].first << ranks[i].second << endl;
            else
                cout << i+1 << ranks[i].first << ranks[i].second << endl;
        }
    }
}