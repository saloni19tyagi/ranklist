import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

class Sortbymarks implements Comparator<RankList> 
{ 

    public int compare(RankList a,RankList  b) 
    { 
        return b.marks - a.marks; 
    } 
}  
public class RankList {
String roll;
int marks;
RankList(String roll,int marks){  
this.roll=roll;  
this.marks=marks;
} 
public static void main(String[] args) throws FileNotFoundException {
// TODO Auto-generated method stub
Scanner sc = new Scanner(new File("ranklist.txt"));
ArrayList<RankList> al = new ArrayList<RankList>();
while (sc.hasNext()){
  String str = sc.nextLine();
  StringTokenizer rl= new StringTokenizer(str, " ");
  while(rl.hasMoreTokens()) {
  String roll=rl.nextToken();
  int mark=Integer.parseInt(rl.nextToken());
      al.add((new RankList(roll,mark))); 
  }
}
Collections.sort(al, new Sortbymarks()); 
for(RankList rl: al){  
System.out.println(rl.roll+" "+rl.marks);  
} 
}

}