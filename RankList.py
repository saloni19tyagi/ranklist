def ordered_rank_list(dic_of_marks):

    return dic_of_marks.most_common(len(dic_of_words))

 

def get_words(filename):

    file_handler = open(filename)

    words = tuple()

    for line in file_handler.readlines():

        words = words [ : ] + tuple(line.strip().split(" "))

    #print(words)

    return words

 

def make_dictionary(tuple_of_marks):

    dic_of_marks = {}

    for i in range(0,len(tuple_of_marks),2):

        dic_of_marks[tuple_of_marks[i]] = int(tuple_of_marks[i + 1])

    #print(dic_of_marks)

    return dic_of_marks

 

input_words = get_words("marklist.txt.txt");

print(ordered_rank_list(make_dictionary((input_words))))